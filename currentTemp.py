'''
	This lib is made to extract temperatures from a DS18X20 temperature probe,
	and a WiPy micro-controler
	
	With a DS18X20 probe connected on the WiPy as this :
    	- ground cable on the WiPy's ground pin
    	- positive cable on the WiPy's 3V3 pin
    	- data cable (yellow) on the WiPy's pin #10 (GP10)
		- pull-up between the positive cable and the data cable with a 4,7Kohm resistor

'''

import time
from machine import Pin
from ds18x20 import DS18X20


def tempRead():
    temp = d.read_temps()
    return(temp)


def tempWrite(temp):
    with open('CurrentTemp.txt', 'w') as fp:
    fp.write(temp)


def tempStartRead():
    d = DS18X20(Pin('G10', mode=Pin.OUT))
    while True:
        temp = tempRead()
        tempWrite(temp)
        time.sleep(60)
