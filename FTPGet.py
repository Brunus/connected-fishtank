#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from ftplib import FTP

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # for Python ver. < 3.0


def config_init(iniFile):

    if not os.path.isfile(iniFile):
        print 'Config file not found, please provide one.'
        print 'Stopping script.'
        sys.exit(1)

    else:
        config = ConfigParser()
        config.read(iniFile)
        return dict(config.items('FTP_configuration'))


def get(config_file):

    config = {}  # will receive config params
    config.update(config_init(config_file))

    TARGETFILE = 'RETR ' + config['remotefile']

    ftp = FTP(host=config['host'])
    ftp.login(user=config['user'], passwd=config['passwd'])

    if config['passivemode'] is True:
        ftp.set_pasv(True)

    ftp.cwd(config['remotedir'])
    ftp.retrbinary(TARGETFILE, open(config['outputfile'], 'wb').write)
    ftp.quit()
