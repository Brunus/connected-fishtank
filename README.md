# connected-fishtank
This project is a set of tools I use in the goal of monitoring a ... fish tank.

I'm monitoring my fish tank parameters (water parameters) with a **IoT device embeding MicroPython**.

The device, a **WiPy 2.0**, is already able to extract temperatures from the tank, with the help of a temperature probe.

More probes and controls will come later, like : controling the CO2 production, monitring PH, controling Led ribons etc.

## drivers for DS18X20 temperature probe
I'm cloning in this repository the drivers available in the archive : http://donkey.vernier.se/~yann/onewire.zip

There is several sources for this driver, you need both onewire.py and ds18x20.py in the WiPy lib directory.

There is one release importing also pyb.py library, this release don't work on WiPy 2.0

## FTPGet
Is the library I use to recover datas saved on the WiPy's flash memory : The WiPy is providing FTP and Telnet services.

The WiPy is connected to my home network and my router is providing access (NAT) to it's FTP and Telnet ports.

The script is almost agnostic, you could use it when you need to connect any IoT device providing a FTP service, while you take care of giving it a config file like in the [FTP_configuration] section of ftank-config.ini.

## ftank-temps (need to be renamed)
Is the complete script to update and draw daily, weekly and (not yet) monthly temperature curves

This script is saving one file/week aside of the current week file, the archive is named like this : 2017-3-week-10.json
