#!/usr/bin/env python
# -*- coding: utf-8 -*-

import calendar
import argparse
import json
import pygal
import time
import os
from datetime import date
from pygal.style import DefaultStyle

from FTPGet import get

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # for Python ver. < 3.0

parser = argparse.ArgumentParser(
    description='Ftank-dailyvalues is dedicated to draw daily curves.')
parser.add_argument(
    '-i', '--ini_file', help='configuration file (default: %(default)s)',
    default="config.ini")
args = parser.parse_args()


def config_init(iniFile):
    if not os.path.isfile(iniFile):
        print 'Config file not found, please provide one.'
        print 'Stopping script.'
        sys.exit(1)

    else:
        config = ConfigParser()
        config.read(iniFile)
        y_labels = json.loads(config.get('y_labels', 'values'))
        conf = dict(config.items('basic_configuration'))
        conf.update({'y_labels':[y_labels]})
        return conf

def update_dailyCurve(conf, current_date):

    with open(conf['datas_daily'], 'r') as fp:
        values_daily = json.load(fp)

    with open(conf['datas_hourly'], 'r') as fp:
        temp = fp.read()

    # re-initialising values_daily values if current time is 1
    if current_date['current_time'] is 1:
        values_daily = {'values_daily': [None for idx in range(24)]}

    values_daily['values_daily'][current_date['current_time']] = float(temp)
    with open(conf['datas_daily'], 'w') as fp:
        json.dump(values_daily, fp)
    return(values_daily)


def render_dailyCurve(conf, current_date, values_daily):

    line_chart = pygal.Line(
        fill=False,
        style=DefaultStyle)

    line_chart.title = (
        'Fish Tank temperature curves - '
        + current_date['current_day']
        + ' '
        + current_date['current_monthName']
        + ' '
        + current_date['current_date'])
    
    y_labels = tuple(conf['y_labels'][0])
    
    # -- defining x axis range
    line_chart.x_labels = map(str, range(0, 24))
    line_chart.y_labels = y_labels
    # -- defining values for the current day
    line_chart.add(current_date['current_day'], values_daily['values_daily'])

    # -- rendering curve
    line_chart.render_to_file(conf['output_dir'] + '/' + conf['curve_daily'])


def update_weeklyCurve(conf, current_date, values_daily):

    # re-initialising values_weekly values if current day is monday
    if current_date['current_time'] is 1 and current_date['current_day'] == 'monday':
        week = ['monday', 'tuesday', 'wenesday', 'thursday', 'friday', 'saturday', 'sunday']
        values_weekly = {day: [None for idx in range(24)] for day in week}
    else:
        with open(conf['datas_weekly'], 'r') as fp:
            values_weekly = json.load(fp)

    # in weekly list, keys are like : values_monday
    dkey = 'values_' + current_date['current_day']

    # pasting daily datas in the corresponding day in weekly list
    values_weekly[dkey] = values_daily[u'values_daily']

    # writting current week data
    with open(conf['datas_weekly'], 'w') as fp:
        json.dump(values_weekly, fp)

    # writing an archive file
    # weekly filename : 2017-2-week-6.json
    weekly_filename = (
        str(current_date['current_year'])
        + '-'
        + current_date['current_monthName']
        + '-week-'
        + current_date['current_week']
        + '.json')

    with open(weekly_filename, 'w') as fp:
        json.dump(values_weekly, fp)

    return(values_weekly)


def render_weeklyCurve(conf, current_date, values_weekly):

    # defining title and styles
    line_chart = pygal.Line(
        fill=False,
        style=DefaultStyle)

    line_chart.title = (
        'Fish tank temperature curves - '
        + str(current_date['current_year'])
        + '- week '
        + current_date['current_week']
    )

    y_labels = tuple(conf['y_labels'][0])

    # defining x axis range
    line_chart.x_labels = map(str, range(0, 24))
    line_chart.y_labels = y_labels
    # defining values for each day's line
    line_chart.add('monday', values_weekly[u'values_monday'])
    line_chart.add('tuesday',  values_weekly[u'values_tuesday'])
    line_chart.add('wenesday',  values_weekly[u'values_wenesday'])
    line_chart.add('thursday',  values_weekly[u'values_thursday'])
    line_chart.add('friday',  values_weekly[u'values_friday'])
    line_chart.add('saturday',  values_weekly[u'values_saturday'])
    line_chart.add('sunday',  values_weekly[u'values_sunday'])

    # rendering curve
    line_chart.render_to_file(conf['output_dir'] + '/' + conf['curves_weekly'])


def update_yearlyCurve(conf, current_date, values_daily):
    '''
    # Work in progress...
    values_monthly = {}
    days_inMonth = calendar.monthrange(
        current_date['current_year'],
        current_date['current_month'])[1]
    values_key = 'values_' + current_date['current_monthName']
    values_monthly = {values_key: [None for idx in range(days_inMonth)]}

    average_daily = 0
    days = 0
    for value in values_daily['values_daily']:
        if value != None:
            average_daily += value
        days += 1
    average_daily = average_daily / days

    # yearly curves init
    yearly_curves.update({i: [None for idx in range(calendar.monthrange(datetime.date.today()[0], datetime.date.today()[1])[1])]})

    '''


if __name__ == "__main__":

    current_date = {}
    conf = {}  # will receive config params
    conf.update(config_init(args.ini_file))

    while(True):

        d = date.today()
        t = time.localtime()

        current_date['current_time'] = t.tm_hour
        current_date['current_date'] = str(d.day)
        current_date['current_day'] = calendar.day_name[d.weekday()].lower()
        current_date['current_week'] = str(d.isocalendar()[1])
        current_date['current_month'] = d.month
        current_date['current_monthName'] = d.strftime("%B")
        current_date['current_year'] = d.year

        get('FTPGet-mine.ini')
        values_daily = update_dailyCurve(conf, current_date)
        values_weekly = update_weeklyCurve(conf, current_date, values_daily)
        # values_yearly = update_yearlyCurve(current_date, values_daily)
        render_dailyCurve(conf, current_date, values_daily)
        render_weeklyCurve(conf, current_date, values_weekly)
        time.sleep(3600)
